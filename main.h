#pragma once

#if defined(_MSC_VER) //MSVC
#ifdef _WIN32
#include <Windows.h>
#define crossleep(seconds) Sleep(seconds*1000)
#else
#error "Compilation with MSC was expected to be under Windows..."
#endif

#elif defined(__GNUC__) //GCC
#if defined(__linux__)
#include <unistd.h> 
#define crossleep(seconds) sleep(seconds)
#elif defined(__WIN32__) || defined(__WIN32)
#include <Windows.h>
#define crossleep(seconds) Sleep(seconds*1000)
#else
#error "Can not determine target OS!"
#endif

#elif defined(__clang__) //Clang
#if defined(__unix__)
#include <unistd.h> 
#define crossleep(seconds) sleep(seconds)
#elif defined(__WIN32__) || defined(__WIN32)
#include <Windows.h>
#define crossleep(seconds) Sleep(seconds*1000)
#else
#error "Can not determine target OS!"
#endif

#else //Unknown
#error "Unknown compiler!"
#endif